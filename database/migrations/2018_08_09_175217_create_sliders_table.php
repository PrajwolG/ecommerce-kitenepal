<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagefile');
            $table->string('link')->nullable();
            $table->integer('status')->nullable();
            $table->integer('rank')->nullable();
            $table->string('caption')->nullable();
            $table->string('keywords')->nullable();
            $table->string('metaTag')->nullable();
            $table->string('metaDescription')->nullable();
            


            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
