<?php
namespace App\Classes;
use App\Product;
use App\ProductCategory;
use App\Brand;
use App\ProductType;
use DB;

class Fun
{
	function productType($category)
	{
	        $productType = DB::table('products')
	            ->join('product_types', 'products.productType', '=', 'product_types.id')
	            ->select('product_types.*','products.category')->distinct()
	      		->where([['products.category','=',$category],['products.status','=',1]])
	            ->get();
	            //dd($productType);
	        return $productType;    	
	}

	function brand($pdtype,$category)
	{
	        $brand = DB::table('products')
	            ->join('brands', 'products.brand', '=', 'brands.id')
	            ->select('brands.*','products.productType')->distinct()
	      		->where([['products.category','=',$category],['products.productType','=',$pdtype],['products.status','=',1]])
	            ->get();
	            //dd($productType);
	        return $brand;    	
	}


	function product($catg,$pt)
	{
	        $product = DB::table('products')
	            ->join('product_categories', 'products.category', '=', 'product_categories.id')
	            ->join('product_types','products.productType','=','product_types.id')

	            ->select('products.*')
	      		->where([['products.category','=',$catg],['products.productType','=',$pt],['products.status','=',1]])
	            ->get();
	            //dd($productType);
	        return $product;    	
	}



}
?>