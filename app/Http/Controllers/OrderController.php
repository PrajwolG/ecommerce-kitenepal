<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$pid)
    {
        //
        $order= new Order();
        
        $order->PID=$pid;
        $order->quantity=$request->quantity;

        $order->email=$request->email;
    	$order->fullname=$request->fullname;
    	$order->address=$request->address;
    	$order->district=$request->district;
    	$order->city=$request->city;
    	$order->street=$request->street;
    	$order->tole=$request->tole;
    	$order->wardno=$request->wardno;
    	$order->contactno=$request->contactno;

        $order->save();
        
        $od=DB::table('orders')->select('*')->orderBy('id','desc')->limit(1)->get();
        foreach($od as $row)
        {
        $oid=$row->id;
    	return redirect('email/'.$oid);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function email_update(Request $request, $id)
    {
        //

        $order=Order::find($id);

        $order->email=$request->email;
        $order->save();
        

        return redirect('address/'.$id);
    }

    public function address_update(Request $request, $id)
    {
        //

        $order=Order::find($id);

        $order->fullname=$request->fullname;
        $order->address=$request->address;
        $order->district=$request->district;
        $order->city=$request->city;
        $order->street=$request->street;
        $order->tole=$request->tole;
        $order->wardno=$request->wardno;
        $order->contactno=$request->contactno;

        $order->save();
        

        return redirect('payment/'.$id);
    }

    public function payment_update(Request $request, $id)
    {
        //

        $order=Order::find($id);

        $order->confirm=1;
        
        $order->save();
        
        return redirect('confirm/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function inactivate(Request $request, $id)
    {
        $order=Order::find($id);
        
        $order->delivery=0;

        $order->save();
        return redirect('orderView'); 
    }


    public function activate(Request $request, $id)
    {
        $order=Order::find($id);
       
        $order->delivery=1;
        //dd($gallery);

        $order->save();
        return redirect('orderView'); 
    }





}
