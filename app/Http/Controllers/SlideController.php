<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slider;
use App\Setting;
use Auth;
use Input as Input;
use File;
use Image; 

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $slider=new Slider();

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/slider/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(900, 600);
            $img->save(public_path($dest_path.$filename));

            $image = $dest_path.$filename;
            
            $slider->imagefile=$image;
                
        }

            else
    {
        
    
        return redirect('slider');

    }

        $slider->link=$request->link;
        $slider->status=0;
        $slider->rank=1;
        $slider->caption=$request->caption;
        $slider->keywords=$request->keywords;
        $slider->metaTag=$request->metaTag;
        $slider->metaDescription=$request->metaDescription;

        $slider->save();
        return redirect('slider');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $slider=Slider::paginate(9);
        $result=Slider::find($id);
        $setting=Setting::all();
        //dd($sl);
        return view('back.slider.edit',['slider'=>$slider,'result'=>$result,'setting'=>$setting]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

      
        $slider=Slider::find($id);

        $img=$request->file;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/slider/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(1400, 750);
            $img->save(public_path($dest_path.$filename));

            $slider->imagefile = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

            else
            {
            
            $dest_path = "img/slider/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(900, 600);
            $img->save(public_path($dest_path.$filename));

            $slider->imagefile = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

                
        }
        else
        {
            $slider->imagefile=$request->image;

        }

        $slider->link=$request->link;

        $slider->rank=1;
        $slider->caption=$request->caption;
        $slider->keywords=$request->keywords;
        $slider->metaTag=$request->metaTag;
        $slider->metaDescription=$request->metaDescription;
/*        $slider->user_id=Auth::id();*/

        $slider->save();
        return redirect('slider');
    }

     


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $data=Slider::find($id);
        if(!is_null($data->imagefile)) //image files
        {
            $file_path=$data->imagefile;
            if(file_exists($file_path))
                unlink($file_path);
        }
        
        $data->delete();
        return redirect('slider');


    }

    public function inactivate(Request $request, $id)
    {
        $slider=Slider::find($id);
/*        $slider->user_id=Auth::id();*/
        $slider->status=0;

        $slider->save();
        return redirect('slider'); 
    }


    public function activate(Request $request, $id)
    {
        $slider=Slider::find($id);
/*        $slider->user_id=Auth::id();*/
        $slider->status=1;
        //dd($slider);

        $slider->save();
        return redirect('slider'); 
    }

/*    public function up(Request $request,$id)
    {
        $slider=Slider::find($id);
        
    }*/


}
