<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Setting;
use App\ProductCategory;
use App\Brand;
use App\ProductType;
use Input as Input;
use File;
use Image; 

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $product= new Product();

        $product->category=$request->category;
        $product->brand=$request->brand;
        $product->productType=$request->producttype;
        $product->modelno=$request->modelno;
        $product->Pcode=$request->Pcode;
        $product->Pname=$request->Pname;
        $product->MRP=$request->MRP;
        $product->SPrice=$request->SPrice;

        //main images
       
        $img=$request->file1;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image1=$dest_path.$filename;
            
           
                
        }

            else
    {
        
    
        return redirect('D-product');

    }


       //main images
       
        $img=$request->file2;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image2=$dest_path.$filename;
            
           
                
        }

            else
    {
        
    
        return redirect('D-product');

    }

       //main images
       
        $img=$request->file3;
        //newly added

        if(!is_null($img))
        { 
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image3=$dest_path.$filename;
            
           
                
        }

            else
    {
        
    
        return redirect('D-product');

    }


        $product->status=0;
        $product->productTitle=$request->productTitle;
        $product->productDescription=$request->productDescription;
        $product->caption=$request->caption;
        $product->keywords=$request->keywords;
        $product->metaTag=$request->metaTag;
        $product->metaDescription=$request->metaDescription;
        $product->save();

        return redirect('D-product');




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting=Setting::all();
        $category=ProductCategory::all();
        $brand=Brand::all();
        $product=Product::paginate(10);
        $result=Product::find($id);
        $producttype=ProductType::all();
        return view('back.product.edit',['product'=>$product,'result'=>$result,'setting'=>$setting,'category'=>$category,'brand'=>$brand,'producttype'=>$producttype]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $product=Product::find($id);

        $product->category=$request->category;
        $product->brand=$request->brand;
        $product->productType=$request->producttype;
        $product->modelno=$request->modelno;
        $product->Pcode=$request->Pcode;
        $product->Pname=$request->Pname;
        $product->MRP=$request->MRP;
        $product->SPrice=$request->SPrice;


/*image1*/
        $img=$request->file1;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image1;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image1 = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

            else
            {
            
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image1 = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

                
        }
        else
        {
            $product->image1=$request->image1;

        }

/*end of image1*/


/*image2*/
        $img=$request->file2;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image2;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image2 = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

            else
            {
            
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image2 = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

                
        }
        else
        {
            $product->image2=$request->image2;

        }

/*end of image 2*/ 


/*image 3*/
        $img=$request->file3;
        //newly added

        if(!is_null($img))
        { 
            $file_path= $request->image3;
            
            if(file_exists($file_path))
            {
            
            unlink($file_path);
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image3 = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

            else
            {
            
            $dest_path = "img/product/";
            $filename =uniqid()."-".$img->getClientOriginalName();
            
            //resize image
            $img = Image::make($img->getRealPath());
            $img->resize(600, 600);
            $img->save(public_path($dest_path.$filename));

            $product->image3 = $dest_path.$filename;
            
            //$slider->imagefile=$image;
            
            }

                
        }
        else
        {
            $product->image3=$request->image3;

        }


/*end of image 3*/






        $product->productTitle=$request->productTitle;
        $product->productDescription=$request->productDescription;
        $product->caption=$request->caption;
        $product->keywords=$request->keywords;
        $product->metaTag=$request->metaTag;
        $product->metaDescription=$request->metaDescription;
        $product->save();

        return redirect('D-product');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $product=Product::find($id);
        if(!is_null($product->image1)) //image files
        {
            $file_path1=$product->image1;
            if(file_exists($file_path1))
                unlink($file_path1);
        }

        if(!is_null($product->image2)) //image files
        {
            $file_path2=$product->image2;
            if(file_exists($file_path2))
                unlink($file_path2);
        }

        if(!is_null($product->image3)) //image files
        {
            $file_path3=$product->image1;
            if(file_exists($file_path3))
                unlink($file_path3);
        }

        $product->delete();
        return redirect('viewproduct');


    }


    public function inactivate(Request $request, $id)
    {
        $product=Product::find($id);

        $product->status=0;

        $product->save();
        return redirect('viewproduct'); 
    }


    public function activate(Request $request, $id)
    {
        $product=Product::find($id);

        $product->status=1;
        //dd($product);

        $product->save();
        return redirect('viewproduct'); 
    }


}


