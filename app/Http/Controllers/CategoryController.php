<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use Auth;
use App\Setting;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pcategory= new ProductCategory();

        $pcategory->category=$request->category;
        $pcategory->status=0;

        $pcategory->rank=$request->rank;
        $pcategory->caption=$request->caption;
        $pcategory->keywords=$request->keywords;
        $pcategory->metaTag=$request->metaTag;
        $pcategory->metaDescription=$request->metaDescription;

        $pcategory->save();

        return redirect('D-Category');





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $category=ProductCategory::paginate(10);
        $result=ProductCategory::find($id);
        $setting=Setting::all();
        //dd($sl);
        return view('back.productCategory.edit',['category'=>$category,'result'=>$result,'setting'=>$setting]);
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $pcategory=ProductCategory::find($id);

        $pcategory->category=$request->category;
        $pcategory->status=0;

        $pcategory->rank=$request->rank;
        $pcategory->caption=$request->caption;
        $pcategory->keywords=$request->keywords;
        $pcategory->metaTag=$request->metaTag;
        $pcategory->metaDescription=$request->metaDescription;

        $pcategory->save();

        return redirect('D-Category');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $pcategory=ProductCategory::find($id);
        $pcategory->delete();
        return redirect('D-Category');




    }


   public function inactivate(Request $request, $id)
    {
        $pcategory=ProductCategory::find($id);
        
        $pcategory->status=0;

        $pcategory->save();
        return redirect('D-Category'); 
    }


    public function activate(Request $request, $id)
    {
        $pcategory=ProductCategory::find($id);
        
        $pcategory->status=1;
        //dd($testimonial);

        $pcategory->save();
        return redirect('D-Category'); 
    }
}
