<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\Brand;
use App\Slider;
use App\Setting;
use App\ProductType;
use App\Order;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider=Slider::all()->where("status","=","1");
        $product=Product::all()->where("status","=","1");
        $product1=DB::table('products')->select('*')->where("status","=","1")->orderBy('id', 'desc')->limit(5)->get();
        $category=ProductCategory::all()->where("status","=","1");
        $brand=Brand::all()->where("status","=","1");
        $setting=setting::all();
        return view('front.home',['slider'=>$slider,'product'=>$product,'product1'=>$product1,'brand'=>$brand,'category'=>$category,'setting'=>$setting]);
    }

    public function product($id)
    {
        $product=Product::find($id);
        $pd=Product::all()->where("status","=","1");
        $setting=setting::all();
        return view('front.product.index',['product'=>$product,'setting'=>$setting,'pd'=>$pd]);
    }

    public function checkout()
    {
        $setting=setting::all();
        return view('front.checkout.index',['setting'=>$setting]);
    }

    public function reg()
    {
        $setting=setting::all();
        return view('front.register.index',['setting'=>$setting]);
    }


    public function email($id)
    {
        $order=Order::find($id);
        $setting=setting::all();
        return view('front.email.index',['setting'=>$setting,'order'=>$order]);
    }


    public function address($id)
    {   
        $order=Order::find($id);
        $setting=setting::all();
        return view('front.address.index',['setting'=>$setting,'order'=>$order]);
    }


    public function payment($id)
    {   
        $order=Order::find($id);
        $setting=setting::all();
        return view('front.payment.index',['setting'=>$setting,'order'=>$order]);
    }

    public function confirm($id)
    {   
        $confirm=DB::table('orders')
        ->join('products','orders.PID','=','products.id')
        ->select('orders.*','products.Pname','products.image1','products.SPrice')
        ->where('orders.id','=',$id)->get();
        $setting=setting::all();
        return view('front.confirm.index',['setting'=>$setting,'confirm'=>$confirm]);
    }


}
