<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductType;
use Auth;
use App\Setting;



class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        //
        $producttype= new ProductType();

        $producttype->productType=$request->producttype;
        $producttype->status=0;

        $producttype->save();

        return redirect('D-producttype');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $producttype=ProductType::all();
        $result=ProductType::find($id);
        $setting=Setting::all();
        return view('back.producttype.edit',['result'=>$result,'producttype'=>$producttype,'setting'=>$setting]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $producttype=ProductType::find($id);

        $producttype->productType=$request->producttype;
        $producttype->save();
        return redirect('D-producttype');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=ProductType::find($id);
        $data->delete();
        return redirect('D-producttype');
    }


   public function inactivate(Request $request, $id)
    {
        $producttype=ProductType::find($id);
        
        $producttype->status=0;

        $producttype->save();
        return redirect('D-producttype'); 
    }


    public function activate(Request $request, $id)
    {
        $producttype=ProductType::find($id);
        
        $producttype->status=1;
        //dd($testimonial);

        $producttype->save();
        return redirect('D-producttype'); 
    }

}
