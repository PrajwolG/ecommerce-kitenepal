<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use Auth;
use Input as Input;
use App\Setting;



class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**s
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $brand= new Brand();
        $brand->brandname=$request->brandname;
        $brand->status=0;

        $brand->save();

         return redirect('D-brand');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $brand=Brand::all();
        $result=Brand::find($id);
        $setting=Setting::all();
        return view('back.brand.edit',['result'=>$result,'brand'=>$brand,'setting'=>$setting]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $brand=Brand::find($id);

        $brand->brandname=$request->brandname;
        $brand->save();
        return redirect('D-brand');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data=Brand::find($id);
        $data->delete();
        return redirect('D-brand');
    }


   public function inactivate(Request $request, $id)
    {
        $brand=Brand::find($id);
        
        $brand->status=0;

        $brand->save();
        return redirect('D-brand'); 
    }


    public function activate(Request $request, $id)
    {
        $brand=Brand::find($id);
        
        $brand->status=1;
        //dd($testimonial);

        $brand->save();
        return redirect('D-brand'); 
    }
}
