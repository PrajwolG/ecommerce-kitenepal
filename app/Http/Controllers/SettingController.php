<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Auth;
use Input as Input;
use File;
use Image;  // for resize of image


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $setting=Setting::find($id);
        //dd($setting);
/*        $setting->user_id=Auth::id();*/
        $setting->company=$request->company;
        $setting->address=$request->address;
        $setting->contact=$request->contact;
        $setting->email=$request->email;
        $setting->google_map=$request->google_map;
        $setting->facebook=$request->facebook;
        $setting->youtube=$request->youtube;
        $setting->google_plus=$request->googleplus;
        $setting->twitter=$request->twitter;
        $setting->instagram=$request->instagram;
        $setting->caption=$request->caption;
        $setting->keywords=$request->keywords;
        $setting->metaTag=$request->metaTag;
        $setting->metaDescription=$request->metaDescription;
        //$group->metaDesc(table_column)=$request->metaDesc(form feild);
        
        $logo=$request->file;
        //newly added

        if(!is_null($logo))
        {
            $file_path= $request->logo_pic;
            if(file_exists($file_path))
            {
                unlink($file_path);
                $dest_path = "img/setting/";
                $filename =uniqid()."-".$logo->getClientOriginalName();
                
                //resize image
                $logo = Image::make($logo->getRealPath());
                $logo->resize(360, 360);
                $logo->save(public_path($dest_path.$filename));

                //$logo->move($dest_path,$filename);
                $image = $dest_path.$filename;
                $setting->logo=$image;
            }
            else
            {
                 $dest_path = "img/setting/";
                 $filename =uniqid()."-".$logo->getClientOriginalName();
                 //resize image
                 $logo = Image::make($logo->getRealPath());
                 $logo->resize(360, 360);
                 $logo->save(public_path($dest_path.$filename));
                 //$logo->move($dest_path,$filename);
                 $image = $dest_path.$filename;
                 $setting->logo=$image;
             }        
        }
        else
        {
            $setting->logo=$request->logo_pic;
            // if(Input::hasFile('file'))
            // {
            //     $file=Input::file('file');
            //     $img_path='img/setting/';
            //     $image=$file->getClientOriginalName();
            //     $file->move('img/setting',$image);
            //     $dest=$img_path.$image;
            //     $setting->logo=$dest;
            // }
        }

        $setting->save();
        return redirect('setting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
