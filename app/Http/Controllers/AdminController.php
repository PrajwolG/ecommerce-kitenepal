<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\ProductCategory;
use App\Product;
use App\User;
use App\Slider;
use App\Setting;
use App\ProductType;
use App\Order;
use DB;
use auth;
class AdminController extends Controller
{
    //

        public function __construct()
    {
        $this->middleware('auth');
    }


    public function login()
    {
        return view('auth.login');
    }


    public function logout()
    {
        auth::logout();
        return view('auth.login');
    }



	public function admin()
    {

        $setting=setting::all();
    	return view('back.admin',['setting'=>$setting]);
    }

     public function user()
    {
        
        $user=User::paginate(10);
        $setting=setting::all();
        return view('back.user.create',['rows'=>$user,'setting'=>$setting]);

    }


    public function brand()
    {
        $brand=Brand::all();
        $setting=setting::all();
    	return view('back.brand.index',['brand'=>$brand,'setting'=>$setting]);
	}

    public function producttype()
    {
        $producttype=ProductType::all();
        $setting=Setting::all();
        return view('back.producttype.index',['producttype'=>$producttype,'setting'=>$setting]);
    }


    public function productCategory()
    {
    	$category=ProductCategory::paginate(10);
        $setting=setting::all();
        return view('back.productCategory.create',['category'=>$category,'setting'=>$setting]);
	}

    public function product()
    {
    	$product=Product::all();
        $brand=Brand::all()->where ("status","=","1");
        $category=ProductCategory::all()->where ("status","=","1");
        $setting=setting::all();
        $producttype=ProductType::all()->where ("status","=","1");
        return view('back.product.index',['product'=>$product,'brand'=>$brand,'category'=>$category,'setting'=>$setting,'producttype'=>$producttype]);
	}

    public function viewproduct()
    {
        $product=Product::all();
        $setting=setting::all();
        return view('back.product.view',['product'=>$product,'setting'=>$setting]);
    }



        public function slider()
    {
        
        $slider=Slider::paginate(9);
        //dd($sl);
        $setting=setting::all();
        return view('back.slider.create',['slider'=>$slider,'setting'=>$setting]);
    }

     public function setting()
    {
        $setting=setting::all();
        return view('back.setting.index',['rows'=>$setting],['setting'=>$setting]);
    }



     public function orderView()
    {
        $setting=setting::all();
       /* $order=Order::all();*/

        $order=DB::table('orders')
        ->join('products','orders.PID','=','products.id')
        ->select('orders.*','products.Pname','products.image1','products.SPrice')
        ->get();        

        return view('back.orderView.index',['setting'=>$setting,'order'=>$order]);
    }


     public function orderViewview($id)
    {
        $setting=setting::all();
       /* $order=Order::all();*/

        $order=DB::table('orders')
        ->join('products','orders.PID','=','products.id')
        ->select('orders.*','products.*')
        ->where('orders.id','=',$id)
        ->get();        

        return view('back.orderView.view',['setting'=>$setting,'order'=>$order]);
    }





	
}
