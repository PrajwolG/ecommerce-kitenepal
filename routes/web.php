<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home'); 
});


//front
Route::get('/','HomeController@index')->name('home');
Route::get('/home','HomeController@index');

/*Route::get('/checkout','HomeController@checkout')->name('checkout');
*/

Route::get('/product/{id}','HomeController@product')->name('product');
Route::post('/order/store/{id}','OrderController@store')->name('/store');
Route::get('/email/{id}','HomeController@email')->name('/email');
Route::post('/order/email_update/{id}','OrderController@email_update')->name('/email_update');
Route::get('/address/{id}','HomeController@address')->name('address');
Route::post('/order/address_update/{id}','OrderController@address_update')->name('address_update');
Route::get('/payment/{id}','HomeController@payment')->name('payment');
Route::post('/order/payment_update/{id}','OrderController@payment_update')->name('payment_update');
Route::get('/confirm/{id}','HomeController@confirm')->name('confirm');



Route::get('/reg','HomeController@reg')->name('reg');
Route::post('/reg/store','CustomerLoginController@store')->name('/store');



//admin
Route::get('/admin','AdminController@admin')->name('admin');
Route::get('/login','AdminController@login')->name('login');
Route::get('/logout','AdminController@logout')->name('logout');

Auth::routes();

//brand
Route::get('/D-brand','AdminController@brand')->name('brand');
Route::post('/D-brand/store','BrandController@store')->name('/store');
Route::get('/D-brand/destroy/{id}','BrandController@destroy')->name('D-brand.destroy');
Route::get('/D-brand/edit/{id}','BrandController@edit')->name('D-brand.edit');
Route::post('/D-brand/update/{id}','BrandController@update')->name('D-brand.update');
Route::get('/D-brand/activate/{id}','BrandController@activate')->name('D-brand.activate');
Route::get('/D-brand/inactivate/{id}','BrandController@inactivate')->name('D-brand.inactivate');

//producttype
Route::get('/D-producttype','AdminController@producttype')->name('producttype');
Route::post('/D-producttype/store','ProductTypeController@store')->name('/store');
Route::get('/D-producttype/destroy/{id}','ProductTypeController@destroy')->name('D-producttype.destroy');
Route::get('/D-producttype/edit/{id}','ProductTypeController@edit')->name('D-producttype.edit');
Route::post('/D-producttype/update/{id}','ProductTypeController@update')->name('D-producttype.update');
Route::get('/D-producttype/activate/{id}','ProductTypeController@activate')->name('D-producttype.activate');
Route::get('/D-producttype/inactivate/{id}','ProductTypeController@inactivate')->name('D-producttype.inactivate');



//productCategory
Route::get('/D-Category','AdminController@productCategory')->name('D-CategoryCategory');
Route::post('/D-Category/store','CategoryController@store')->name('/store');
Route::get('/D-Category/destroy/{id}','CategoryController@destroy')->name('/D-Category.destroy');
Route::get('/D-Category/edit/{id}','CategoryController@edit')->name('/D-Category.edit');
Route::post('/D-Category/update/{id}','CategoryController@update')->name('/D-Category.update');
Route::get('/D-Category/activate/{id}','CategoryController@activate')->name('D-Category.activate');
Route::get('/D-Category/inactivate/{id}','CategoryController@inactivate')->name('D-Category.inactivate');

//product
Route::get('/D-product','AdminController@product')->name('product');
Route::post('/D-product/store','ProductController@store')->name('/store');
Route::get('viewproduct','AdminController@viewproduct')->name('/viewproduct');
Route::get('/D-product/destroy/{id}','ProductController@destroy')->name('/D-product.destroy');
Route::get('/D-product/edit/{id}','ProductController@edit')->name('/D-product.edit');
Route::post('/D-product/update/{id}','ProductController@update')->name('/D-product.update');
Route::get('/D-product/activate/{id}','ProductController@activate')->name('/D-product.activate');
Route::get('/D-product/inactivate/{id}','ProductController@inactivate')->name('/D-product.inactivate');


//user
Route::get('/user','AdminController@user')->name('user');
Route::post('/user/store','UserController@store')->name('store');
Route::get('/user/edit/{id}','UserController@edit')->name('user.edit');
Route::post('/user/update/{id}','UserController@update')->name('user.update');
Route::get('/user/delete/{id}','UserController@destroy')->name('user.destroy');

//slider
Route::get('/slider','AdminController@slider')->name('slider');
Route::post('/slider/store','SlideController@store')->name('store');
Route::get('/slider/edit/{id}','SlideController@edit')->name('slider.edit');
Route::post('/slider/update/{id}','SlideController@update')->name('slider.update');
Route::get('/slider/delete/{id}','SlideController@destroy')->name('slider.destroy');
Route::get('/slider/activate/{id}','SlideController@activate')->name('slider.activate');
Route::get('/slider/inactivate/{id}','SlideController@inactivate')->name('slider.inactivate');


//setting
Route::get('/setting','AdminController@setting')->name('setting');
Route::post('/setting/update/{id}','SettingController@update')->name('update');


//Order View
Route::get('/orderView','AdminController@orderView')->name('orderView');
Route::get('/orderView/activate/{id}','OrderController@activate')->name('orderView.activate');
Route::get('/orderView/inactivate/{id}','OrderController@inactivate')->name('orderView.inactivate');
Route::get('/orderViewview/{id}','AdminController@orderViewview')->name('orderViewview');

