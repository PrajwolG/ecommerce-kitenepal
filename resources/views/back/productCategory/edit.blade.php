@extends('back.productCategory.index')
  @section('category')<!--main content start-->
    <div class="col-lg-4">
        <!-- testimonial Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>Testimonial Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of testimonial Bar -->

        <!-- testimonial Management Start-->
          <!-- SEO Management Start -->
        <form action="{{url('/D-Category/update')}}/{{$result->id}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">


                <!-- Category -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Category</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Category" name="category" value="{{$result->category}}">
                  </div>
                </div>
                <!-- End of Category -->

                <br>
              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->
        <!-- End of Category Management -->


<br>


        <!-- SEO Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>SEO Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of SEO Bar -->

        <!-- SEO Management Start -->
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">
               
                <br>

                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" value="{{$result->caption}}">
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keywords" rows="5">{{$result->keywords}}</textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" value="{{$result->metaTag}}" >
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5">{{$result->metaDescription}}</textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
                <br>
                <button class="btn btn-default">Save</button>
              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->

        </form>


    </div>    
    @endsection