@extends('back.layout.master')
@section('content')
  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | Setting</li>
                </div>
              </div>
            </ol>
          </div>
        </div>

        @foreach($rows as $data)
        <form action="{{url('/setting/update')}}/{{$data->id}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
        <div class="row">
          <!-- Left Column Start -->
            <!-- Company Detail Start -->

          <div class="col-lg-8">
            <div class="form-wrapper well">
              <div class="form-group">
                
                <!-- Company Start -->
                <div class="row">
                  <div class="col-sm-3">
                      <label for="text">Company Name</label>
                  </div>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" id="text" placeholder="Enter Company Name" name="company" value="{{$data->company}}">
                  </div>
                </div>
                <!-- End of Company -->
                <br>
                
                <!-- Address Start -->
                <div class="row">
                  <div class="col-sm-3">
                      <label for="text">Address</label>
                  </div>
                  <div class="col-sm-9">
                      <textarea class="form-control" id="text" placeholder="Enter Full Address" name="address" rows="5">{{$data->address}}</textarea>
                  </div>
                </div>
                <!-- End of Address -->
                <br>

                <!-- Contact Start -->
                <div class="row">
                  <div class="col-sm-3">
                      <label for="text">Contact Number</label>
                  </div>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" id="text" placeholder="Enter Contact Number" name="contact" value="{{$data->contact}}">
                  </div>
                </div>
                <!-- End of Contact -->
                <br>

                <!-- Email Start -->
                <div class="row">
                  <div class="col-sm-3">
                      <label for="text">Email</label>
                  </div>
                  <div class="col-sm-9">
                      <input type="Email" class="form-control" id="text" placeholder="Enter Email ID" name="email" value="{{$data->email}}">
                  </div>
                </div>
                <!-- End of Email -->
                <br>

              </div>
            </div>

              <!-- End of Company Detail -->

              <br>  

              <!-- Social Links Start -->
                <!-- Social Links Bar -->
              <div class="row">
                  <div class="col-lg-12">
                    <ol class="breadcrumb">
                      <div class="row">
                        <div class="col-md-12">
                          <li><i class="fa fa-share-alt"></i>Social Links</li>
                        </div>
                      </div>
                    </ol>
                  </div>
              </div>
                <!-- End of Social Links Bar -->

                <!-- Social Links Detail -->
              <div class="form-wrapper well">
                <div class="form-group">
                   <!-- Google Map -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Google Map</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Google Map IFrame Link" name="googleMap" value="{{$data->google_map}}">
                    </div>
                  </div>
                  <!-- End of Google Map-->
                  <br> 

                  <!-- Facebook Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Facebook Page</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Facebook Page Link" name="facebook" value="{{$data->facebook}}">
                    </div>
                  </div>
                  <!-- End of Facebook Page-->
                  <br>
                  
                  <!-- Youtube Link -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Youtube Link</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Youtube Link" name="youtubeLink" value="{{$data->youtube}}">
                    </div>
                  </div>
                  <!-- End of Youtube Link -->
                  <br>

                  <!-- Google Plus Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Google Plus Page</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Google Plus Page" name="googlePlus" value="{{$data->google_plus}}">
                    </div>
                  </div>
                  <!-- End of Google Plus -->
                  <br>

                  <!-- Twitter Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Twitter Page</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Twitter Page" name="twitter" value="{{$data->twitter}}">
                    </div>
                  </div>
                  <!-- End of Twitter -->
                  <br>

                  <!-- Instagram Page -->
                  <div class="row">
                    <div class="col-sm-3">
                        <label for="text">Instagram Page</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="text" placeholder="Enter Instagram Page" name="instagram" value="{{$data->instagram}}">
                    </div>
                  </div>
                  <!-- End of Instagram -->
                  <br>

                </div>
              </div>
              <!-- End of Social Link Detail -->
            <!-- End of Social Links -->
          </div>
          <!-- End of Left Column -->

          <!-- Right Column Start -->
          <div class="col-lg-4">
            <!-- SEO Bar Start -->
            <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <li><i class="fa fa-search"></i>SEO Management</li>
                    </div>
                  </div>
                </ol>
              </div>
            </div>
            <!-- End of SEO Bar -->

            <!-- SEO Management Start -->
            <div class="form-wrapper well">
              <div class="form-group">

                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" value="{{$data->caption}}">
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keyword" rows="5">{{$data->keywords}}</textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" value="{{$data->metaTag}}">
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5">{{$data->metaDescription}}</textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
                <br>

              </div>
              </div>
            <!-- End of SEO Management -->
              <br>
              <!-- Company Brand -->
                <!-- Brand Bar Start -->
              <div class="row">
                <div class="col-lg-12">
                  <ol class="breadcrumb">
                    <div class="row">
                      <div class="col-md-12">
                        <li><i class="fa fa-slack"></i>Logo</li>
                      </div>
                    </div>
                  </ol>
                </div>
              </div>
                <!-- End of Brand Bar -->

            <!-- Logo -->
            <div class="form-wrapper well">
              <div class="form-group">
                  <!-- Browse Logo -->
                <div class="row">
                  <div class="thumbnail">
                        <img class="group list-group-image" src="{{$data->logo}}" alt="Company Logo" height="100" width="100" />
                  </div>      
                  <div class="col-sm-2">
                      <label for="text">Logo</label>
                  </div>
                  <div class="col-sm-10">
                    
                    <input type="file" name="file" class="file">
                    <input type="hidden" value="{{$data->logo}}" name="logo_pic">
                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload">
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>

                      <!-- newly added -->
                    <!-- <input type="file" name="file"> -->
                    

                  </div>
                </div>
                <!-- End of Browse Logo -->
                <br>
              </div>
            </div>
            <!-- End of Logo-->
              <!-- End of Company Brand -->
          </div>
          <!-- End of Right Column -->
        </div>
        <!-- End of Main Row -->   
        
        <br>

        <!-- Button Operation Start -->
        <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <center><button class="btn btn-primary"><b>Update</b></button></center>
                    </div>
                  </div>
                </ol>
              </div>
        </div>
      </form>
      @endforeach
        <!-- End of Button Operation -->

        </div>
      </section>
    </section>
@endsection
