@extends('back.layout.master')
@section('content')
  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | Service</li>             
                </div>
                <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
                

                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('D-product')}}"><i class="fa fa-plus"></i>Add</a></li>
                </div>
             

              </div>
            </ol>


            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Product</th>
                    <th>Selling Price</th>
                    <th>Status</th>

                    <th colspan="3" style="text-align: center;">Setting</th>
                    
                  </tr>
                </thead>
                <tbody>
                  
                  @foreach($product as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->Pname}}</td>
                    <td>{{$data->SPrice}}</td>



                    <td>
                      @if($data->status==1)
                      <a href="{{'D-product/inactivate'}}/{{$data->id}}">
                        <div class="btn btn-default" style=" background-color:red;">InActivate</div>
                      </a>
                      @else
                      <a href="{{'D-product/activate'}}/{{$data->id}}">
                        <div class="btn btn-default" style=" background-color:lightgreen;">Activate</div>
                      </a>
                      @endif
                    </td> 

                    <td style="text-align: center;"><a href="{{url('/D-product/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="{{('D-product/destroy')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>  


                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
            <center>

            </center>

            

          </div>



        </div>


      </section>
    </section>
    <!-- container section start -->
@endsection