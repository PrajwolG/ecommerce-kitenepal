@extends('back.layout.master')
  @section('content')<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        
      <!-- main bar start -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <div class="row">
              <div class="col-md-6">
                <li><i class="fa fa-home"></i><a href="{{'admin'}}">Home</a> | Product</li>
              </div>

              <div class="col-md-6">
                  <li class="text-right">
                
                    <a href="{{url('viewproduct')}}"><i class="fa fa-eye"></i>View Produtcts</a></li>
              </div>

            </div>
          </ol>
        </div>
      </div>
      <!-- end main bar -->

      <!-- main content start -->
      <form action="{{url('/D-product/store')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}      
      <div class="row">
               
          <!-- Left Column Start -->
            <!-- About Start -->

          <div class="col-lg-8">
            <div class="form-wrapper well">
              <div class="form-group">

              <!-- category -->
              <div class="row">  
              <div class="col-sm-4">
                  <label for="text">Category</label>
              </div>


              <div class="col-sm-8">                  
               <select class="form-control" id="text" name="category">
                @foreach($category as $data)
                <option value="{{$data->id}}">{{$data->category}}</option>
                @endforeach

               </select>
               
               </div>
             </div>
               <!-- end of category -->
              <br>

              <!-- brand -->
              <div class="row">  
              <div class="col-sm-4">
                  <label for="text">Brand</label>
              </div>


              <div class="col-sm-8">                  
               <select class="form-control" id="text" name="brand">
                @foreach($brand as  $data)
                <option value="{{$data->id}}">{{$data->brandname}}</option>
<!--                 <option value="2">LENEVO</option>
                <option value="3">HP</option> -->
                  @endforeach
               </select>
               
               </div>
             </div>
              <!-- end of brand -->
              <br>

              <!-- product Type -->
              <div class="row">  
              <div class="col-sm-4">
                  <label for="text">Product Type</label>
              </div>


              <div class="col-sm-8">                  
               <select class="form-control" id="text" name="producttype">
                @foreach($producttype as  $data)
                <option value="{{$data->id}}">{{$data->productType}}</option>
<!--                 <option value="2">LENEVO</option>
                <option value="3">HP</option> -->
                  @endforeach
               </select>
               
               </div>
             </div>
              <!-- end of brand -->
              <br>


              <!-- Model No-->
              <div class="row">
              <div class="col-sm-4">
                  <label for="text">Model No</label>
              </div>

              <div class="col-sm-8">
              <input type="text" class="form-control" id="text" placeholder="Enter Model No" name="modelno" >
              </div>
            </div>
            <!-- End of Model No. -->
            <br>


                <!-- Product Code-->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Product Code</label>
                  </div>

                  <div class="col-sm-8">
                  <input type="text" class="form-control" id="text" placeholder="Enter Product Code" name="Pcode" >
                  </div>
                </div>
                <!-- End of Product Code -->
                <br>

                  <!-- Product Name-->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Product Name</label>
                  </div>

                  <div class="col-sm-8">
                  <input type="text" class="form-control" id="text" placeholder="Enter Product Name" name="Pname" >
                  </div>
                </div>
                <!-- End of Product Name -->
                <br>


                  <!-- Market Price-->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Market Price</label>
                  </div>

                  <div class="col-sm-8">
                  <input type="text" class="form-control" id="text" placeholder="Enter Market Price" name="MRP" >
                  </div>
                </div>
                <!-- End of Market Price -->
                <br>


                  <!-- Selling Price-->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Selling Price</label>
                  </div>

                  <div class="col-sm-8">
                  <input type="text" class="form-control" id="text" placeholder="Enter Selling Price" name="SPrice" >
                  </div>
                </div>
                <!-- End of Selling Price -->
                <br>

                  <!-- Main Image-->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Main Image</label>
                  </div>

                  <div class="col-sm-8">
                <!-- Image Upload Start -->
                    <input type="file" name="file1" class="file">

                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload">
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>
                <!-- End of Image Upload -->


                  </div>
                </div>
                <!-- End of Main Image -->
                <br>


                  <!--Image 1 -->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Image 1</label>
                  </div>

                  <div class="col-sm-8">
                <!-- Image Upload Start -->
                    <input type="file" name="file2" class="file">

                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload">
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>
                <!-- End of Image Upload -->


                  </div>
                </div>
                <!-- End of Image 1  -->
                <br>

                  <!--Image 2-->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Image 2</label>
                  </div>

                  <div class="col-sm-8">
                <!-- Image Upload Start -->
                    <input type="file" name="file3" class="file">

                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload">
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>
                <!-- End of Image Upload -->


                  </div>
                </div>
                <!-- End of Image 2 -->
                <br>

                  <!-- Product Title-->
                  <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Product Title</label>
                  </div>

                  <div class="col-sm-8">
                  <input type="text" class="form-control" id="text" placeholder="Enter Product Title" name="productTitle" >
                  </div>
                </div>
                <!-- End of Product Title -->
                <br>



                <!-- Product Description -->
                  <label for="text">Product Description</label>
                  <textarea class="ckeditor" name="productDescription"></textarea>
                <!-- End of Product Description -->
                <br>




              </div>
            </div>

              <!-- End of Products -->

              <br>  
          </div>



          <!-- End of Left Column -->

            
            <!-- Right Column Start -->
              <!-- @yield('D-D-Product') -->

        <div class="col-lg-4">
        <!-- SEO Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>SEO Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of SEO Bar -->

        <!-- SEO Management Start -->
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">
               
                <br>

                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" >
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keywords" rows="5"></textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" >
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5"></textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->
        
    </div>    
            <!-- End of Right Column -->        
      </div> 
      <!-- Button Operation Start -->
        <div class="row">
              <div class="col-lg-12">
                <ol class="breadcrumb">
                  <div class="row">
                    <div class="col-md-12">
                      <center><button class="btn btn-primary"><b>Update</b></button></center>
                    </div>
                  </div>
                </ol>
              </div>
        </div>

             <!-- End of main content -->
             </form>
      </section>
    </section>


    @endsection