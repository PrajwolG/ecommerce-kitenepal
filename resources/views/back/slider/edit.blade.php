@extends('back.slider.index')
  @section('slider')<!--main content start-->
    
        <div class="col-lg-4">
        <!-- Slider Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>Slider Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of Slider Bar -->



            <!-- SEO Management Start -->
            <form action="{{url('/slider/update')}}/{{$result->id}}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
            
            <div class="row">
             <div class="form-wrapper well">
               <div class="form-group">

                <!-- Image Upload Start -->

                    <center><img src="{{url($result->imagefile)}}" height="80" width="100"></center>
                    <br>
                    <input type="file" name="file" class="file" >
                    <input type="hidden" value="{{$result->imagefile}}" name="image">
                    <div class="input-group col-sm-12">
                      <span class="input-group-addon"><i class="fa fa-image"></i></span>
                      <input type="text" class="form-control input-sm" disabled placeholder="Upload" >
                      <span class="input-group-btn">
                      <button class="browse btn btn-primary input-sm" type="button"><i class="fa fa-search"></i> Browse</button>
                    </span>
                    </div>
                  <br>
                <!-- End of Image Upload -->

                <!-- Link -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Link</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter link" name="link" value="{{$result->link}}">
                  </div>
                </div>
               <!--  End of Link -->
                <br>

              </div>
                  </div>
                    <br>
                    </div> 

                  
               <!-- SEO Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>SEO Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of SEO Bar -->
           
        <!-- SEO Management Start -->
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">
               
                <br>
                
                <!-- Caption -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Caption</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Caption" name="caption" value="{{$result->caption}}">
                  </div>
                </div>
                <!-- End of Caption -->
                <br>
                
                <!-- Keywords -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Keywords</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Keywords" name="keywords" rows="5">{{$result->keywords}}</textarea>
                  </div>
                </div>
                <!-- End of Keywords -->
                <br>

                <!-- Meta Tag -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Tag</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Meta Tag" name="metaTag" value="{{$result->metaTag}}">
                  </div>
                </div>
                <!-- End of Meta Tag -->
                <br>

                <!-- Meta Description -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Meta Description</label>
                  </div>
                  <div class="col-sm-8">
                      <textarea class="form-control" id="text" placeholder="Enter Meta Description" name="metaDescription" rows="5">{{$result->metaDescription}}</textarea>
                  </div>
                </div>
                <!-- End of Meta Description -->
                <br>
                <button class="btn btn-default">Update</button>
              </div>
              </div>
              </form>      
            <!-- End of SEO Management -->
              <br>
          </div>

    @endsection