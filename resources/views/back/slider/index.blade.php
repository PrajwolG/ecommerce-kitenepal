@extends('back.layout.master')
  @section('content')<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        
      <!-- main bar start -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <div class="row">
              <div class="col-md-6">
                <li><i class="fa fa-home"></i><a href="{{'admin'}}">Home</a> | Slider</li>
              </div>

                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('slider')}}"><i class="fa fa-plus"></i>Add Slide</a></li>
                </div>
            </div>
          </ol>
        </div>

      </div>
      <!-- end main bar -->

      <!-- main content start -->
      <div class="row">
        
        
              <!--  -->
          <div class="col-md-8">    
            <div class="container">
              <div class="form-group">
                <div class="container">
                  <div class="well well-sm">
                    <strong>Display</strong>
                    <div class="btn-group">
                      <a href="#" class="btn btn-default btn-sm" id="list"><span class="fa fa-th-list"></span> List</a>
                      <a href="#" class="btn btn-default btn-sm" id="grid"><span class="fa fa-th"></span> Grid</a>
                    </div>
                  </div>

                  <div class="showgrid" id="showgrid">
                    
                    <div id="products" class="row list-group">
                        @foreach($slider as $data)
                      <div class="item col-xs-12 col-lg-4">
                        <div class="thumbnail">
                          <img class="group list-group-image" src="{{url($data->imagefile)}}"  alt="" />
                          <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                          <center>{{$data->link}}</center></h4>
                          </div>


                          <div class="row">
                            <div class="col-xs-12 col-md-12">
                              <table class="table table-responsive">
                                <tr>
                                  <td style="text-align: center;"><a href="{{url('/slider/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                  <td style="text-align: center;"><a href="{{('slider/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                  
                                  <td>
                                    @if($data->status==1)
                                    <a href="{{'slider/inactivate'}}/{{$data->id}}">
                                      <div class="btn btn-default" style=" background-color:red;">InActivate</div>
                                    </a>
                                    @else
                                    <a href="{{'slider/activate'}}/{{$data->id}}">
                                      <div class="btn btn-default" style=" background-color:lightgreen;">Activate</div>
                                    </a>
                                    @endif
                                  </td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                        @endforeach
                    </div>
                  </div>
                </div>
              </div>
              <div class="showlist" id="showlist">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Image</th>
                        <th>Image Name</th>
                        <th colspan="3" style="text-align: center;">Setting</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($slider as $data)
                      <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td><img src="{{url($data->imagefile)}}" height="80" width="100"></td>
                        <td>{{$data->link}}</td>
                        <td style="text-align: center;"><a href="{{url('/slider/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                        <td style="text-align: center;"><a href="{{url('/slider/destroy')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <script>

              </script>
              <center>
                <div class="col-lg-12">
                  {!! $slider->render() !!}
                </div>

              </center>
            </div>            
        </div>
            <!-- End of Left Column -->
            
            <!-- Right Column Start -->
              @yield('slider')
            <!-- End of Right Column -->        
      </div> 
             <!-- End of main content -->
      </section>
    </section>


    @endsection