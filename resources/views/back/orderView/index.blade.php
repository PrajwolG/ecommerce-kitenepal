@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | Orders</li>
                </div>
              </div>
            </ol>
           <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Contact No</th>
                    <th>Confirm</th>
                    <th>Delivery</th>
                    <th>Order Item</th>
                    <th colspan="3" style="text-align: center;">Setting</th>


                  </tr>
                </thead>
                <tbody>
                  
                  @foreach($order as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->fullname}}</td>
                    <td>{{$data->email}}</td>
                    <td>{{$data->address}}</td>

                    <td>{{$data->contactno}}</td>

                    <td> @if($data->confirm==1)
                                  
                      <div class="btn btn-default" style=" background-color:lightgreen;">Confirmed</div>
                    
                    @else
                
                      <div class="btn btn-default" style=" background-color:red;">Not Confirmed</div>
                    
                       @endif

                     </td>


                <td>
                  @if($data->delivery==1)
                  <a href="{{'orderView/inactivate'}}/{{$data->id}}">
                    <div class="btn btn-default" style=" background-color:lightgreen;">Delivered</div>
                  </a>
                  @else
                  <a href="{{'orderView/activate'}}/{{$data->id}}">
                    <div class="btn btn-default" style=" background-color:red;">Undelivered</div>
                  </a>
                  @endif
                </td>
                <td><img src="{{url($data->image1)}}" height="80" width="100"><br>{{$data->Pname}}</td>
                <td style="text-align: center;"><a href="{{url('/orderViewview')}}/{{$data->id}}"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>



					  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>


      </section>
    </section>







@endsection