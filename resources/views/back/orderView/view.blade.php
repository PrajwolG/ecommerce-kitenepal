@extends('back.layout.master')
@section('content')
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{url('admin')}}">Home</a> | Orders</li>
                </div>

                <div class="col-md-6">
                <li class="text-right"><i class="fa fa-eye"></i><a href="{{url('orderView')}}">View All Orders</a></li>
                </div>

              </div>
            </ol>
          </div>
        </div>

        <div class="row">
           <div class="table-responsive col-md-6">
              @foreach($order as $data)
              <img src="{{url($data->image1)}}" height="300" width="500">
              @endforeach
              <table class="table">
                <thead>
                  <tr>
                    <th>Product Details</th>
                  </tr>
                </thead>
                
                <tbody>
                  @foreach($order as $data)
                    <tr>
                    <td>Product Name:</td>
                    <td>{{$data->Pname}}</td>
                    </tr> 

                    <tr>
                    <td>Market Price:</td>
                    <td>{{$data->MRP}}</td>
                    </tr> 

                    <tr>
                    <td>Product Title</td>
                    <td>{{$data->productTitle}}</td>
                    </tr> 

                    <tr>
                    <td>Product Description:</td>

                    <td>
                      
                      {!! $data->productDescription !!}
                      

                    </td>
                    </tr>


                  @endforeach

                </tbody>
              </table>
            </div>


            <div class="table-responsive col-md-6">
              <table class="table">
                <thead>
                  <tr>
                    
                    <th>Customer Details</th>
                  </tr>

                </thead>
                <tbody>
                  
                  @foreach($order as $data)
                    <tr>
                    <td>Full Name:</td>
                    <td>{{$data->fullname}}</td>
                    </tr> 

                    <tr>
                    <td>Email:</td>
                    <td>{{$data->email}}</td>
                    </tr> 

                    <tr>
                    <td>Address</td>
                    <td>{{$data->address}}</td>
                    </tr> 


                    <tr>
                    <td>District</td>
                    <td>{{$data->district}}</td>
                    </tr> 
                    
                    <tr>
                    <td>City</td>
                    <td>{{$data->city}}</td>
                    </tr> 

                    <tr>
                    <td>Street</td>
                    <td>{{$data->street}}</td>
                    </tr> 

                    <tr>
                    <td>Tole</td>
                    <td>{{$data->tole}}</td>
                    </tr> 

                    <tr>
                    <td>Ward No</td>
                    <td>{{$data->wardno}}</td>
                    </tr> 

                    <tr>
                    <td>Contact Number</td>
                    <td>{{$data->contactno}}</td>
                    </tr> 


                  @endforeach

                </tbody>
              </table>
            </div>
        </div> 
      </section>
    </section>







@endsection