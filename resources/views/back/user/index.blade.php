@extends('back.layout.master')
@section('content')
  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-8">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | User</li>
                </div>

                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('user')}}"><i class="fa fa-plus"></i>Add</a></li>
                </div>

                <!-- <li><i class="fa fa-laptop"></i>Dashboard</li> -->
                

<!--                 <div class="col-md-6">
                  <li class="text-right"><a href="#"><i class="fa fa-plus"></i>Add</a></li>
                </div> -->
             

              </div>
            </ol>


            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>User Name</th>
                    <th>Email ID</th>
                    <th colspan="3" style="text-align: center;">Setting</th>
                  </tr>
                </thead>
                <tbody>
                  
                  @foreach($rows as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->name}}</td>
                    <td>{{$data->email}}</td>

                    <td style="text-align: center;"><a href="{{url('/user/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="{{('user/delete')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
                {!! $rows->render() !!}

              </div>
            </center>

            

          </div>

 @yield('user')

        </div>


      </section>
    </section>
    <!-- container section start -->
@endsection