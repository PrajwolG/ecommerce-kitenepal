@extends('back.user.index')
@section('user')

<div class="col-lg-4">
        <!-- User Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>User Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of User Bar -->

        <!-- User Management Start-->
         
        <form action="{{url('/user/update')}}/{{$result->id}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">


                <br>
                <!-- Full Name -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Full Name</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter fullname" name="fullname" value="{{$result->name}}" >
                  </div>
                </div>
                <!-- End of Full Name -->

                <br>
                
                <!-- Email -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Email</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Email" name="email"  value="{{$result->email}}">
                  </div>
                </div>
                <!-- End of Email -->

                <br>

                <!-- Password -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Password</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="password" class="form-control" id="text" placeholder="Enter Password" name="password"  >
                  </div>
                </div>
                <!-- End of Password -->

                <br>

				<center><button class="btn btn-default">Add</button></center>
                
              </div>
            </div>   
          <br>
        </div>
        
        <!-- End of User Management -->

        
        </form>
    </div>    


@endsection