@extends('back.layout.master')
  @section('content')<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li><i class="fa fa-home"></i><a href="index.html">Home</a> | Form</li>
                </div>
                <div class="col-md-6">
                  <li class="text-right"><a href="#"><i class="fa fa-plus"></i>Add</a></li>
                </div>
              </div>
            </ol>
            <div class="form-wrapper well">
              <div class="form-group">
                <label for="email">Title:</label>
                <input type="email" class="form-control" id="email" placeholder="Title goes here..." name="email">
                <br>
                <label for="comment">Description:</label>
                  <textarea name="description" class="ckeditor"></textarea>

                <br>
                <label for="sel1">Type:</label>
                <select class="form-control" id="sel1">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                </select>
                <br>
                <label for="comment">Choose one:</label>
                <div class="btn-group">
                  <label class="radio-inline">
                    <input type="radio" name="optradio"> Option 1</label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio"> Option 2</label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio"> Option 3</label>
                </div>
                <br>
                <label for="comment">Choose any:</label>
                <div class="btn-group">
                  <label class="checkbox-inline">
                    <input type="checkbox" value=""> Option 1</label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value=""> Option 2</label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value=""> Option 3</label>
                </div>
              </div>
              <button class="btn btn-primary"><b>Save</b></button>
              <button class="btn btn-default">Cancel</button>
            </div>
          </div>
        </div>
      </section>
    </section>
    <!-- container section start -->
    @endsection