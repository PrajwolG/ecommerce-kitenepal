@extends('back.layout.master')
  @section('content')<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li><i class="fa fa-home"></i><a href="index.html">Home</a> | Multimedia</li>
                </div>
                <div class="col-md-6">
                  <li class="text-right"><a href="#"><i class="fa fa-plus"></i>Add</a></li>
                </div>
              </div>
            </ol>
            <div class="container">
              <div class="form-group">
                <input type="file" name="img[]" class="file">
                <div class="input-group col-xs-12">
                  <span class="input-group-addon"><i class="fa fa-image"></i></span>
                  <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                  <span class="input-group-btn">
                  <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                </span>
                </div>
                <div class="container">
                  <div class="well well-sm">
                    <strong>Display</strong>
                    <div class="btn-group">
                      <a href="#" class="btn btn-default btn-sm" id="list"><span class="fa fa-th-list"></span> List</a>
                      <a href="#" class="btn btn-default btn-sm" id="grid"><span class="fa fa-th"></span> Grid</a>
                    </div>
                  </div>
                  <div class="showgrid" id="showgrid">
                    <div id="products" class="row list-group">
                      <div class="item col-xs-12 col-lg-4">
                        <div class="thumbnail">
                          <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                          <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                          Image's file name</h4>
                          </div>
                          <div class="row">
                            <div class="col-xs-12 col-md-12">
                              <table class="table table-responsive">
                                <tr>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="item col-xs-12 col-lg-4">
                        <div class="thumbnail">
                          <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                          <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                          Image's file name</h4>
                          </div>
                          <div class="row">
                            <div class="col-xs-12 col-md-12">
                              <table class="table table-responsive">
                                <tr>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="item col-xs-12 col-lg-4">
                        <div class="thumbnail">
                          <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                          <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                          Image's file name</h4>
                          </div>
                          <div class="row">
                            <div class="col-xs-12 col-md-12">
                              <table class="table table-responsive">
                                <tr>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="item col-xs-12 col-lg-4">
                        <div class="thumbnail">
                          <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                          <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                          Image's file name</h4>
                          </div>
                          <div class="row">
                            <div class="col-xs-12 col-md-12">
                              <table class="table table-responsive">
                                <tr>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="item  col-xs-12 col-lg-4">
                        <div class="thumbnail">
                          <img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
                          <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                          Image's file name</h4>
                          </div>
                          <div class="row">
                            <div class="col-xs-12 col-md-12">
                              <table class="table table-responsive">
                                <tr>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="showlist" id="showlist">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Image Name</th>
                        <th>Image Description</th>
                        <th colspan="3" style="text-align: center;">Setting</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1</th>
                        <td>img001.jpg</td>
                        <td>Image description goes here.</td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                      </tr>
                      <tr>
                        <th scope="row">2</th>
                        <td>img002.jpg</td>
                        <td>Image description goes here.</td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                      </tr>
                      <tr>
                        <th scope="row">3</th>
                        <td>img003.jpg</td>
                        <td>Image description goes here</td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                        <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <script>

              </script>
              <center>
                <div class="col-lg-12">
                  <ul class="pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                  </ul>
                </div>
              </center>
            </div>
            <button class="btn btn-default">Publish</button>
            <button class="btn btn-default">Cancel</button>
      </section>
    </section>
    <!-- container section start -->
    @endsection