@extends('back.layout.master')
  @section('content')<section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li><i class="fa fa-home"></i><a href="index.html">Home</a> | User</li>
                </div>
                <div class="col-md-6">
                  <li class="text-right"><a href="#"><i class="fa fa-plus"></i>Add</a></li>
                </div>
              </div>
            </ol>
          
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>S.N.</th>
                  <th>Page Title</th>
                  <th>Description</th>
                  <th colspan="3" style="text-align: center;">Setting</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Home</td>
                  <td>Main page of your company.</td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>About</td>
                  <td>About page of your company.</td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Service</td>
                  <td>Service page of your company.</td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                  <td style="text-align: center;"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>
                </tr>
              </tbody>
            </table>
          </div>  
          </div>
          <center>
            <div class="col-lg-12">
               <ul class="pagination">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
              </ul>
            </div> 
          </center>                  
        </div>
        <button class="btn btn-default">Publish</button>
        <button class="btn btn-default">Cancel</button>
      </section>
    </section>
    <!-- container section start -->
    @endsection