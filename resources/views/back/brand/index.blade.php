@extends('back.layout.master')
@section('content')
  <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-8">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-6">
                  <li class="text-left"><i class="fa fa-home"></i><a href="{{('admin')}}">Home</a> | Brand</li>
                </div>
                
                

                <div class="col-md-6">
                  <li class="text-right"><a href="{{url('D-brand')}}"><i class="fa fa-plus"></i>Add</a></li>
                </div>
             

              </div>
            </ol>


            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.N.</th>
                    <th>Brand</th>
                    <th>Status</th>
                    <th colspan="3" style="text-align: center;">Setting</th>
                  
                  </tr>
                </thead>
                <tbody>


                  @foreach($brand as $data)
                  <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <th>{{$data->brandname}}</th>
                    <td>
                      @if($data->status==1)
                      <a href="{{'D-brand/inactivate'}}/{{$data->id}}">
                        <div class="btn btn-default" style=" background-color:red;">InActivate</div>
                      </a>
                      @else
                      <a href="{{'D-brand/activate'}}/{{$data->id}}">
                        <div class="btn btn-default" style=" background-color:lightgreen;">Activate</div>
                      </a>
                      @endif
                    </td>
                    <td style="text-align: center;"><a href="{{url('/D-brand/edit')}}/{{$data->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></td>
                    <td style="text-align: center;"><a href="{{('D-brand/destroy')}}/{{$data->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></td>

                   </tr> 

                   @endforeach
                  

                </tbody>
              </table>
            </div>
            <center>
              <div class="col-lg-12">
               

              </div>
            </center>

            

          </div>

 <!-- @yield('brand') -->

     <div class="col-lg-4">
        <!-- brand Bar Start -->
        <div class="row">
          <div class="col-lg-12">
            <ol class="breadcrumb">
              <div class="row">
                <div class="col-md-12">
                  <li><i class="fa fa-search"></i>Brand Management</li>
                </div>
              </div>
            </ol>
          </div>
        </div>
        <!-- End of brand Bar -->

        <!-- brand Management Start-->
          <!-- SEO Management Start -->
         
        <form action="{{url('/D-brand/store')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
        <div class="row">  
            <div class="form-wrapper well">
              <div class="form-group">

               
                <!-- Brand  -->
                <div class="row">
                  <div class="col-sm-4">
                      <label for="text">Brand</label>
                  </div>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="text" placeholder="Enter Brand" name="brandname" >
                  </div>
                </div>
                <!-- End of Brand-->
                <br>
                <br>
                <center><button class="btn btn-default">Save</button></center>

              </div>
            </div>   
          <br>
        </div>
        <!-- End of SEO Management -->
        <!-- End of brand Management -->


        </form>
        </div> 

        </div>


      </section>
    </section>
    <!-- container section start -->
@endsection