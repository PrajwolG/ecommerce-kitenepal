@extends('front.layout.master')
@section('content')
  <!-- products -->
  <div class="container-fluid">
    <div class="row product">
      <div class="col-md-5">
        <div class="col-sm-3 sub_img" id="sub_img" onclick="changeImage(event)">


          <div>
            <img src="{{url('')}}/{{$product->image1}}" alt="" width="100%" id="1">
           </div>
           <br>

           <div> 
            <img src="{{url('')}}/{{$product->image2}}" alt="" width="100%" id="2">
            </div>
            <br>
            
            <img src="{{url('')}}/{{$product->image3}}" alt="" width="100%" id="3">


        </div>
        

         <div class="col-sm-9">
          
          <img id="mainImage" src="{{url('')}}/{{$product->image1}}" alt="" width="100%" data-toggle="magnify">   

        </div>



      </div>

      <form action="{{url('/order/store')}}/{{$product->id}}" method="post">
        {{ csrf_field() }}
      <div class="col-md-4">
        <h4>{{$product->productTitle}} </h4>
        <ul type="circle">
          {!! $product->productDescription!!}
        </ul>

<!--         <button class="btn share_btn-twitter"><i class="fa fa-twitter"></i> tweet </button>
        <button class="btn share_btn-facebook"><i class="fa fa-facebook"></i> share </button>
        <button class="btn share_btn-googleplus"><i class="fa fa-google-plus"></i> google + </button>
   -->

<!--          <p class="share-to-friend"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send to a friend</a></p>
 -->


         <div class="quantity_buy">
          <div class="row">
            <div class="col-md-4">
              <input type="number" class="form-control" placeholder="Quantity" min=0 name="quantity">
            </div>
            <div class="col-md-6">
              <h4>Rs. {{$product->SPrice}}</h4> 
              <!-- <button class="cart-btn"><i class="fa fa-cart-plus" aria-hidden="true"></i></button> -->
            </div>
            <div class="col-md-12">
              <button class="btn btn-block">Buy Now</button>
            </div>
          </div>
        </div>
      </div>
    </form>

<!--       <div class="col-md-3 order-on-phone">
        <h3><i class="fa fa-phone-square" aria-hidden="true"></i> Order on Phone</h3>
        <p>Don't have a credit card or bank account? Don't worry. You can still order this item and have it delivered at your doorstep !</p> -->


<!--         <form>
          <div class="form-group">
            <label>Full Name :</label>
            <input type="text" name="fullname" class="form-control">
          </div>
          <div class="form-group">
            <label>Contact :</label>
            <input type="text" name="lastname" class="form-control">
          </div>
          <div class="form-group">
            <label>Email Address :</label>
            <input type="email" name="email" class="form-control">
          </div>
          <input type="submit" value="Send" class="btn">
        </form> -->
<!--       </div> -->
    </div>
  </div>

  
    <!-- Offers -->
  <div class="container">
    <h2><b><u>Related items</b></u></h2>
    <div class="row">

      @foreach($pd as $data)
      @if($product->category==$data->category && $product->id!=$data->id)
      <div class="col-md-2 column productbox two">
        <a href="{{url('product')}}/{{$data->id}}" title="">
          <img src="{{url('')}}/{{$data->image1}}">
          <div class="producttitle">{{$data->Pname}}</div>
          <div class="productprice">
            <div class="pull-right"><a href="{{url('product')}}/{{$data->id}}" class="btn btn-sm" role="button">BUY</a></div>
            <div class="pricetext">Rs. {{$data->SPrice}}</div>
          </div>
        </a>
      </div>
      @endif
      @endforeach      
    </div>
  </div>



  @endsection
