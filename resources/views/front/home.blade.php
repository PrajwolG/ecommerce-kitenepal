<?php
use App\Classes\Fun;
?>
@extends('front.layout.master')
@section('content')
  <div class="container" style="margin-top:20px;max-width:1400px;">
    <!-- Slider -->
    <div class="container">
      <div id="main_area">
        <!-- Slider -->
        <div class="row">
          <div class="col-xs-12" id="slider">
            <div class="row">
              <div class="col-sm-8" id="carousel-bounding-box">
                <div class="carousel slide" id="myCarousel">
                  <!-- Carousel items -->

                  <div class="carousel-inner">
                    @foreach($slider as $data)
                    <div class="@if($data->active==1) active item @else item @endif">
                      <img src="{{$data->imagefile}}">
                    </div>
                    @endforeach

                  </div>
                  <!-- Carousel nav -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="slider__icon">
                    <!-- <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> -->
                  </span>                               
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                  <span class="slider__icon">
                    <!-- <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> -->
                  </span>                                    
                </a> 
                </div>
              </div>
              <div class="col-sm-4">
                <div class="hotdeal">
                  <h3 class="hotdeal--heading text-center">Hot Deal</h3>
                  <img src="https://images-eu.ssl-images-amazon.com/images/G/31/IN-hq/2018/img/Kitchen/XCM_1100842_Manual_670x645_1100842_KitchenStoreRevamp_670x645_02_jpg_Store_revamp_AFT_Tiles.jpg" alt="" class="hotdeal--image">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Offers -->
    <div class="container">
      <div class="row">
      	<div class="col-lg-12">

          @foreach($product1 as $data)
	        <div class="col-md-2 column productbox two ">
	          <a href="{{'product'}}/{{$data->id}}">
              <img src="{{$data->image1}}" class="img-responsive">
              <div class="producttitle"><center>{{$data->Pname}}</center></div>
              <div class="productprice">
                <div class="pull-right"><a href="{{'product'}}/{{$data->id}}" class="btn btn-sm" role="button">BUY</a></div>
                <div class="pricetext">Rs. {{$data->SPrice}}</div>
              </div> 
            </a>
         </div>
         @endforeach


	      </div>
      </div>
    </div>
    <!-- Second Long Menu -->

    <!-- Tab system -->
    <script src="{{url('frontend/js/sidemenu.js')}}"></script>
    
  
    <div class="container">
      <ul class="nav nav-tabs CategoriesMenu">
        
        @foreach($category as $data)
        <li  class="@if($data->active==1) active @endif"><a data-toggle="tab" href="#{{$data->id}}">{{$data->category}}</a></li>
        @endforeach
      </ul>

      <div class="tab-content">
        
        <!-- Computer tab -->
        @foreach($category as $data)
        <div id="{{$data->id}}" class="@if($data->active==1)tab-pane fade in active @else tab-pane fade @endif">
          <div class="container">
            <div class="row">
              <div class="col-md-3 well">
                <?php
                  $pdtype=new Fun;
                  $cid=$data->id;
                  $pt=$pdtype->productType($cid);
                ?>
                <ul class="nav nav-list">
                @foreach($pt as $ptdata)  
                  <li>
                    <label class="tree-toggler nav-header"><a href="#{{$ptdata->id}}"><i class="fa fa-caret-down" aria-hidden="true"></i>{{$ptdata->productType}}</a></label>
                    <ul class="nav nav-list tree">
                      <?php
                        $brand=new Fun;
                        $ptid=$ptdata->id;
                        $cid=$data->id;
                        $bd=$brand->brand($ptid,$cid);
                      ?>
                      @foreach($bd as $bdata)
                      <li><a href="#{{$bdata->id}}"><i class="fa fa-angle-right" aria-hidden="true"></i>{{$bdata->brandname}}</a></li>
                      @endforeach
                    </ul>
                  </li>
                  <li class="divider"></li>
                @endforeach
                </ul>
              </div>
              <!-- End of side menu -->
              <div class="col-md-9">
                <div class="mainItems">
                  <div class="row">
                    @foreach($pt as $ptdata)
                      <?php
                        $catg=new Fun;
                        $cid=$data->id; //product category
                        $ptid=$ptdata->id; //product type
                       $prd=$catg->product($cid, $ptid)
                      ?>
                     @endforeach 

                    @foreach($prd as $data)

                    <div class="col-md-3 col-xs-3 column itembox two firstproduct">
                      <a href="{{'product'}}/{{$data->id}} " >

                      
                        <div class="producttitle">{{$data->Pname}}</div>
                        <img src="{{$data->image1}}" class="img-responsive">
                        <div class="productprice">
                          <div class="pull-right"><a href="{{'product'}}/{{$data->id}}" class="btn btn-sm" role="button">BUY</a></div>
                          <div class="pricetext">Rs. {{$data->SPrice}}</div>
                        </div>
                      </a>

                    </div>

                    @endforeach

                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
        @endforeach
   
      </div>
    </div>
  </div>
  @endsection