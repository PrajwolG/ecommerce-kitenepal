<html>
<head>
	<title></title>
	@include('front.common.header')
</head>
<body>

	@include('front.common.nav')

	@yield('content')

	@include('front.common.footer')

</body>
</html>