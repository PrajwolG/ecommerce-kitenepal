@extends('front.layout.master')
@section('content')

        <div class="checkout--step">
          <div class="row">
            <h1>2. YOUR ADDRESS</h1>
            <br>
            <div class="col-md-8">
              <form method="post" action="{{url('order/address_update')}}/{{$order->id}}">
                {{ csrf_field() }}
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Full Name*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Full Name" name="fullname">
                  </div>
                </div>
              <div class="form-group row">
             <label class="col-sm-2 col-form-label">Address*</label>    
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Address" name="address">
                  </div>
              </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">District*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="District" name="district">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">City*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="City" name="city">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Street </label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Street" name="street">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Tole*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Tole" name="tole">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Ward no.*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Ward no" name="wardno">
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Contact No.*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="+977" name="contactno" >
                  </div>
                </div>
                <button class="btn">SAVE & CONTINUE</button>
              </form>
            </div>
          </div>
        </div>

@endsection