@extends('front.layout.master')
@section('content')

    <!-- checkout form -->
    <section class="section-padding">
      <div class="container">
        <div class="checkout--step">

          <div class="row">
            <form action="{{url('order/email_update')}}/{{$order->id}}" method="POST"> 
              {{ csrf_field() }}
            <h1>1. YOUR EMAIL</h1>
            <div class="col-md-6">
              <strong>What is your email address?</strong><br><br>
              <input type="email" name="email" class="form-control" placeholder="username@gmail.com"><br>
              <button class="btn form-control">PROCEED</button><br><br>
            </div>
            </form>
            <div class="col-md-6">
              <strong>Sign in with your social account</strong><br><br>
              
              <button type="button" class="btn form-control" style="background-color: #3b5998"><i class="fa fa-facebook-square"></i> FACEBOOK</button><br><br>
              <button type="button" class="btn form-control" style="background-color: #1da1f2"><i class="fa fa-twitter-square"></i> TWITTER</button>
            </div>
        
         
          </div>
        </div>
    </div>
</section>

@endsection