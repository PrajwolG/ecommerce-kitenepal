@extends('front.layout.master')
@section('content')

    <!-- checkout form -->
    <section class="section-padding">
      <div class="container">
        <div class="checkout--step">
          <div class="row">
            <h1>1. YOUR EMAIL</h1>
            <div class="col-md-6">
              <strong>What is your email address?</strong><br><br>
              <input type="email" name="" class="form-control" placeholder="username@gmail.com"><br>
              <button type="button" class="btn form-control">PROCEED</button><br><br>
            </div>
            <div class="col-md-6">
              <strong>Sign in with your social account</strong><br><br>
              
              <button type="button" class="btn form-control" style="background-color: #3b5998"><i class="fa fa-facebook-square"></i> FACEBOOK</button><br><br>
              <button type="button" class="btn form-control" style="background-color: #1da1f2"><i class="fa fa-twitter-square"></i> TWITTER</button>
            </div>
          </div>
        </div>
        <div class="checkout--step">
          <div class="row">
            <h1>2. YOUR ADDRESS</h1>
            <div class="col-md-12">
              <form>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Full Name*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Full Name">
                  </div>
                </div>
                <strong>Address</strong><hr>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">District*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="District">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">City*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="City">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Street*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Street">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Tole*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Tole">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Ward no.*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Ward no">
                  </div>
                </div>
                <hr>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Contact No.*</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="+977">
                  </div>
                </div>
                <button type="button" class="btn">SAVE & CONTINUE</button>
              </form>
            </div>
          </div>
        </div>
        <div class="checkout--step">
          <div class="row">
            <h1>3. PAYMENT OPTIONS</h1>
            <div class="col-md-12">
              <h3><mark>Cash on Delivery</mark></h3>
              <p>Pay cash at your doorstep at the time of order delivery.</p>
              <p><strong>Imortant:</strong> Please have the exact amount available as the delivery rider may not be carrying sufficient change.</p>
              <p><strong>Imortant:</strong> This is the last step. Once you click "CONFIRM ORDER", you will not be able to change or edit. To cancel, edit or </p>
              <button type="button" class="btn">CONFIRM ORDER</button>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /checkout form -->




@endsection
