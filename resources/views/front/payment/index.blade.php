@extends('front.layout.master')
@section('content')


        <div class="checkout--step">
          <div class="row">
            <h1>3. PAYMENT OPTIONS</h1>
            <form method="post" action="{{url('order/payment_update')}}/{{$order->id}}">
              {{ csrf_field() }}
            <div class="col-md-12">
              <h3><mark>Cash on Delivery</mark></h3>
              <p>Pay cash at your doorstep at the time of order delivery.</p>
              <p><strong>Imortant:</strong> Please have the exact amount available as the delivery rider may not be carrying sufficient change.</p>
              <p><strong>Imortant:</strong> This is the last step. Once you click "CONFIRM ORDER", you will not be able to change or edit. To cancel, edit or </p>
              <button class="btn">CONFIRM ORDER</button>
            </div>
            </form>
          </div>
        </div>
      </div>
      <br>
      <br>

@endsection