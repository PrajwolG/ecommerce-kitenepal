@extends('front.layout.master')
@section('content')


        <div class="checkout--step">
          <div class="row">
            <h1>Congratulation</h1>
            
            <div class="col-md-8">
              <h3><mark>Your order has been successful</mark></h3>
              <p>We will contact you very soon</p>
              <p><strong>Imortant:</strong> Please have the exact amount available as the delivery rider may not be carrying sufficient change.</p>
              <p><strong>Imortant:</strong> This is the last step. Once you click "CONFIRM ORDER", you will not be able to change or edit. To cancel, edit or </p>
              <a href="{{url('home')}}"><button class="btn"><< Back to Home Page</button></a>
            </div>

            @foreach($confirm as $data)
            <div class="col-md-4">
                <div class="producttitle">{{$data->Pname}}</div>
                  <img src="{{url('')}}/{{$data->image1}}" class="img-responsive" height="200" width="200">
                  <div class="productprice">
                    <div class="pricetext">Rs. {{$data->SPrice}} per quantity</div>
                    <div class="pricetext">Total order Qty : {{$data->quantity}}</div>
                    <?php
                    $price=$data->SPrice;
                    $qty=$data->quantity;
                    $total=$price*$qty;
                    ?>
                    <div class="pricetext">Total Amount : Rs. <?php echo $total?> </div>
                </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>

@endsection