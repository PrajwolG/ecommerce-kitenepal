
@foreach($setting as $data)
  <!-- Navigation bar -->
  <nav class="navbar navbar-default md--navbar">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-header" href="{{url('home')}}"><img src="{{url('')}}/{{($data->logo)}}" width="30" height="30" alt="logo"></a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <div class="together">
          <div class="form-center">
            <form class="navbar-form navbar-left md--form">
              <div class="search-bar">
                <span class="search-icon">
                <i class="fa fa-search"></i>
                <input type="text" class="search" placeholder="Search for anything">
              </span>
              </div>
            </form>
<!--             <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categories <span class="fa fa-sort-down md--fa" aria-hidden="true"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Computer</a></li>
                  <li><a href="#">Cosmetic</a></li>
                  <li><a href="#">Kitchen</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">One more separated link</a></li> 
                </ul>
              </li>
            </ul> -->
          </div>
        </div>
        
<!--         <div id="navbarCollaps" class="navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><span class="fa fa-question-circle-o" aria-hidden="true"></span> Help</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-user-circle-o" aria-hidden="true"> </span> Account <span class="caret"></span></a>
              <ul id="login-dp" class="dropdown-menu">
                <li>
                  <div class="row">
                    <div class="col-md-12">
                      <center>Login with</center>
                      <div class="social-buttons">
                        <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                        <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                      </div>
                      <center>or</center>
                      <br>
                      <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                        <div class="form-group">
                          <label class="sr-only" for="exampleInputEmail2">Email address</label>
                          <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                        </div>
                        <div class="form-group">
                          <label class="sr-only" for="exampleInputPassword2">Password</label>
                          <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                          <div class="help-block text-right"><a href="">Forget the password ?</a></div>
                        </div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-block btn-signin">Sign in</button>
                        </div>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox"> Keep me logged-in
                          </label>
                        </div>
                      </form>
                    </div>
                    <div class="bottom text-center">
                      New here ? <a href="{{url('reg')}}"><b>Join Us</b></a>
                    </div>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div> -->
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>
  @endforeach