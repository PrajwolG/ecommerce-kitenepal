
  <script type="text/javascript" src="{{url('forntend/js/jquery-3.1.1.min.js')}}"></script>
  <script type="text/javascript" src="{{url('frontend/js/slider.js')}}"></script>
  <script type="text/javascript" src="{{url('frontend/js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('frontend/js/product.js')}}"></script>



 @foreach($setting as $data)
  <!--footer-->
  <footer class="bottom-footer">
    <div class="container">
      <div class="row">
        <!-- row -->
        <div class="col-lg-4 col-md-4">
          <!-- widgets column left -->
          <ul class="list-unstyled clear-margins">
            <!-- widgets -->
            <li class="widget-container widget_nav_menu">
              <!-- widgets list -->
              <h1 class="title-widget">Associated With: </h1>
              <ul>
                <li><a href="#">Amatya Store</a></li>
                <li><a href="#">Adarsha</a></li>
                <li><a href="#">KITE Nepal</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- widgets column left end -->
        <div class="col-lg-4 col-md-4">
          <!-- widgets column center -->
          <ul class="list-unstyled clear-margins">
            <!-- widgets -->
            <li class="widget-container widget_recent_news">
              <!-- widgets list -->
              <h1 class="title-widget">Contact Detail </h1>
              <div class="footerp">
                <h2 class="title-median">{{$data->company}}</h2>
                <p><b>Address: </b>{{$data->address}}</p>
                <p><b>Email Id:</b> <a href="info@kitenepal.com">{{$data->email}}</a></p>
                <p><b>Contact Number: </b>+977 {{$data->contact}}</p>
              </div>
            </li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="social-icons">
            <ul class="nomargin">
              <ul class="social-network social-circle">
                <li><a href="{{$data->facebook}}" target="_blank" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="{{$data->twitter}}" target="_blank" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
<!--                 <li><a href="https:\\www.linkedin.com\kitenepal" target="_blank" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li> -->
                <li><a href="{{$data->google_plus}}" target="_blank" class="icoGoogle " title="Google + "><i class="fa fa-google-plus "></i></a></li>
                <li><a href="{{$data->youtube}}" class="icoyoutube " title="youtube "><i class="fa fa-youtube "></i></a></li>
                <li><a href="{{$data->instagram}}" class="icoinstagram " title="instagram"><i class="fa fa-instagram "></i></a></li>

              </ul>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--header-->
  <div class="footer-bottom ">
    <div class="container ">
      <div class="row ">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
          <div class="copyright ">
            © 2017, KITE Nepal, All rights reserved
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
          <div class="design ">
            <a target="_blank " href="http://www.kitenepal.com ">Web Design & Development by KITE Nepal</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach

