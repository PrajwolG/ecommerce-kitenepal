@foreach($setting as $data)
<meta charset="UTF-8">
  <meta name=viewport content="width=device-width, initial-scale=1">
  <title>KN-shop</title>
  <link href="{{url('')}}/{{($data->logo)}}" rel="icon">
  <link rel="stylesheet" type="text/css" href="{{url('frontend/stylesheets/bootstrap.min.css')}}">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <script type="text/javascript" src="{{url('frontend/js/jquery-3.1.1.min.js')}}"></script>
  <script type="text/javascript" src="{{url('frontend/js/slider.js')}}"></script>
  <script type="text/javascript" src="{{url('frontend/js/bootstrap.min.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{url('frontend/stylesheets/main.css')}}">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
@endforeach