<!-- @extends('layouts.app')

@section('content') -->
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->



<!-- @endsection -->

<!-- Copy all the link like Css from header section -->
  <link rel="shortcut icon" href="{{url('backend/img/favicon.png')}}">

  <title>Login</title>

  <!-- Bootstrap CSS -->
  <link href="{{url('backend/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="{{url('backend/css/bootstrap-theme.css')}}" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="{{url('backend/css/elegant-icons-style.css')}}" rel="stylesheet" />
  <link href="{{url('backend/css/font-awesome.css')}}" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="{{url('backend/css/style.css')}}" rel="stylesheet">
  <link href="{{url('backend/css/style-responsive.css')}}" rel="stylesheet" />

  <link rel="stylesheet" href="{{url('backend/stylesheets/module/login.css')}}">
  <script src="{{url('backend/js/login.js')}}"></script>


<body>

  <div class="wrapper">
    <form class="login" method="post" action={{route('login')}}  >  <!-- direct to login page -->
        @csrf   <!-- transfer data -->
      <p class="title">Log in</p>
      <input type="text" placeholder="Email" name="email" autofocus/>
      <i class="fa fa-user"></i>
      <input type="password" placeholder="Password" name="password"/>
      <i class="fa fa-key"></i>
      <a href="#">Forgot your password?</a>
      <button>
        <i class="spinner"></i>
        <span class="state">Log in</span>
      </button>
    </form>
    <footer><a target="blank" href="http://kitenepal.com/">KITE Nepal</a></footer>
    </p>
  </div>
</body>