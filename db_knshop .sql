-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2018 at 02:46 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_knshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brandname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brandname`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dell', 1, '2018-08-06 05:59:30', '2018-08-10 06:55:05'),
(2, 'Lenovo', 1, '2018-08-06 07:07:04', '2018-08-10 06:55:11'),
(3, 'HP', 1, '2018-08-06 07:07:09', '2018-08-10 06:55:09'),
(4, 'SAMSUNG', 1, '2018-08-09 03:46:31', '2018-08-15 04:32:25'),
(5, 'Sony', 0, '2018-08-09 12:52:10', '2018-08-09 12:52:10'),
(6, 'Other', 1, '2018-08-09 12:52:52', '2018-08-10 06:55:13'),
(7, 'BALTRA', 1, '2018-08-14 06:52:17', '2018-08-14 06:52:21'),
(8, 'PHILIPS', 1, '2018-08-15 04:17:54', '2018-08-15 04:17:57');

-- --------------------------------------------------------

--
-- Table structure for table `customer_logins`
--

CREATE TABLE `customer_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmpassword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agree` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_logins`
--

INSERT INTO `customer_logins` (`id`, `firstname`, `lastname`, `displayname`, `email`, `password`, `confirmpassword`, `agree`, `created_at`, `updated_at`) VALUES
(1, 'Prajwol', 'Ghemosu', 'PrajwolG', 'Prajwolghemosu399@gmail.com', '123456', '123456', 1, '2018-08-14 03:27:45', '2018-08-14 03:27:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2018_08_02_093503_create_brands_table', 1),
(14, '2018_08_02_093643_create_products_table', 1),
(15, '2018_08_02_093658_create_product_categories_table', 1),
(16, '2018_07_12_165802_create_sliders_table', 2),
(17, '2018_07_12_170851_create_settings_table', 2),
(18, '2018_07_12_171130_create_abouts_table', 2),
(19, '2018_07_13_094559_create_galleries_table', 2),
(20, '2018_07_26_105134_create_teams_table', 2),
(21, '2018_07_27_000136_create_services_table', 2),
(22, '2018_07_27_001145_create_ourclients_table', 2),
(23, '2018_07_27_001817_create_ourclientsextras_table', 2),
(24, '2018_07_27_002818_create_careers_table', 2),
(25, '2018_07_27_121310_create_testimonials_table', 2),
(26, '2018_07_29_193617_create_contact_uses_table', 2),
(27, '2018_07_29_193946_create_user_careers_table', 2),
(28, '2018_07_31_123925_create_client_groups_table', 2),
(29, '2018_08_09_175217_create_sliders_table', 3),
(30, '2018_08_09_181412_create_settings_table', 4),
(31, '2018_08_13_091654_create_orders_table', 5),
(32, '2018_08_13_100835_create_product_types_table', 6),
(33, '2018_08_14_085228_create_customer_logins_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `PID` int(11) DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8mb4_unicode_ci,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tole` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wardno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirm` int(11) DEFAULT NULL,
  `delivery` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `PID`, `quantity`, `email`, `fullname`, `address`, `district`, `city`, `street`, `tole`, `wardno`, `contactno`, `confirm`, `delivery`, `created_at`, `updated_at`) VALUES
(1, 10, '6', 'prajwolghms@gmail.com', 'Prazol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-08-16 08:24:41', '2018-08-17 05:02:51'),
(2, 11, '3', 'P@gmail.com', 'Avatar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2018-08-16 09:17:47', '2018-08-17 05:16:10'),
(3, 12, '3', 'kitenepal@gmail.com', 'Kite', 'BKT', 'BKT', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2018-08-17 05:14:06', '2018-08-17 05:14:23'),
(4, 1, '1', 'prajwolghms@gmail.com', 'Prazol', 'BKT', NULL, NULL, NULL, NULL, NULL, '981111asda', NULL, NULL, '2018-08-17 05:45:26', '2018-08-17 05:47:51'),
(5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-17 06:55:38', '2018-08-17 06:55:38'),
(6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-17 06:55:45', '2018-08-17 06:55:45'),
(7, 12, '1', 'a@gmail.com', 'Avatar2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2018-08-17 06:58:53', '2018-08-17 07:00:14');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `Pcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Pname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MRP` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SPrice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productTitle` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `productDescription` longtext COLLATE utf8mb4_unicode_ci,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modelno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productType` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `Pcode`, `Pname`, `MRP`, `SPrice`, `image1`, `image2`, `image3`, `productTitle`, `productDescription`, `brand`, `modelno`, `category`, `productType`, `status`, `caption`, `keywords`, `metaTag`, `metaDescription`, `created_at`, `updated_at`) VALUES
(1, '11111', 'Laptop', '50000', '45000', 'img/product/5b6d802c5d8ac-1.jpg', 'img/product/5b756b383ddb2-samsung.jpg', 'img/product/5b6d802c750cd-1.jpg', 'Dell Inspiron i5 5559', '<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\" type=\"circle\">\r\n	<li style=\"box-sizing: border-box;\">Indiglo Night-Light</li>\r\n	<li style=\"box-sizing: border-box;\">Tide Tracker Counts Down to High or Low Tide</li>\r\n	<li style=\"box-sizing: border-box;\">Digital Thermometer with Analog Display Measures Air and Water Temperature</li>\r\n	<li style=\"box-sizing: border-box;\">Accurate Digital Compass with Analog Display</li>\r\n	<li style=\"box-sizing: border-box;\">Adjustable Declination Angle for Accuracy</li>\r\n	<li style=\"box-sizing: border-box;\">Water resistant to 330 feet(100 M)</li>\r\n</ul>', '3', 'D1', '1', 2, '1', NULL, NULL, NULL, NULL, '2018-08-10 06:23:12', '2018-08-16 06:31:57'),
(2, '22222', 'Kitchen Ware', '2500', '1500', 'img/product/5b6d80aa33d2a-2.jpg', 'img/product/5b6d80aa42b8f-2.jpg', 'img/product/5b6d80aa5130a-2.jpg', 'kitchen ware 22222 B1', '<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\" type=\"circle\">\r\n	<li style=\"box-sizing: border-box;\">Indiglo Night-Light</li>\r\n	<li style=\"box-sizing: border-box;\">Tide Tracker Counts Down to High or Low Tide</li>\r\n	<li style=\"box-sizing: border-box;\">Digital Thermometer with Analog Display Measures Air and Water Temperature</li>\r\n	<li style=\"box-sizing: border-box;\">Accurate Digital Compass with Analog Display</li>\r\n	<li style=\"box-sizing: border-box;\">Adjustable Declination Angle for Accuracy</li>\r\n	<li style=\"box-sizing: border-box;\">Water resistant to 330 feet(100 M)</li>\r\n</ul>', '7', 'B1', '2', 3, '1', NULL, NULL, NULL, NULL, '2018-08-10 06:25:18', '2018-08-14 07:07:40'),
(3, '33333', 'New Grinder', '2000', '1500', 'img/product/5b6d8106a9634-3.jpg', 'img/product/5b6d8106b5eaa-3.jpg', 'img/product/5b6d8106c6783-3.jpg', 'Grinder Model no 5546', '<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\" type=\"circle\">\r\n	<li style=\"box-sizing: border-box;\">Indiglo Night-Light</li>\r\n	<li style=\"box-sizing: border-box;\">Tide Tracker Counts Down to High or Low Tide</li>\r\n	<li style=\"box-sizing: border-box;\">Digital Thermometer with Analog Display Measures Air and Water Temperature</li>\r\n	<li style=\"box-sizing: border-box;\">Accurate Digital Compass with Analog Display</li>\r\n	<li style=\"box-sizing: border-box;\">Adjustable Declination Angle for Accuracy</li>\r\n	<li style=\"box-sizing: border-box;\">Water resistant to 330 feet(100 M)</li>\r\n</ul>', '7', 'c1', '2', 3, '1', NULL, NULL, NULL, NULL, '2018-08-10 06:26:50', '2018-08-14 07:22:16'),
(4, '44444', 'Lakme', '1000', '800', 'img/product/5b6d81aa050a3-4.jpg', 'img/product/5b6d81aa13829-4.jpg', 'img/product/5b6d81aa20825-4.jpg', 'Lakme C1 44444', '<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\" type=\"circle\">\r\n	<li style=\"box-sizing: border-box;\">Indiglo Night-Light</li>\r\n	<li style=\"box-sizing: border-box;\">Tide Tracker Counts Down to High or Low Tide</li>\r\n	<li style=\"box-sizing: border-box;\">Digital Thermometer with Analog Display Measures Air and Water Temperature</li>\r\n	<li style=\"box-sizing: border-box;\">Accurate Digital Compass with Analog Display</li>\r\n	<li style=\"box-sizing: border-box;\">Adjustable Declination Angle for Accuracy</li>\r\n	<li style=\"box-sizing: border-box;\">Water resistant to 330 feet(100 M)</li>\r\n</ul>', '5', 'c1', '3', 3, '1', NULL, NULL, NULL, NULL, '2018-08-10 06:29:34', '2018-08-14 07:16:24'),
(5, '55555', 'MSI HeadPhone', '2000', '1500', 'img/product/5b6d81df30f20-5.jpg', 'img/product/5b6d81df3e12e-5.jpg', 'img/product/5b6d81df4a906-5.jpg', 'MSI Headphone 5656 E!', '<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\" type=\"circle\">\r\n	<li style=\"box-sizing: border-box;\">Indiglo Night-Light</li>\r\n	<li style=\"box-sizing: border-box;\">Tide Tracker Counts Down to High or Low Tide</li>\r\n	<li style=\"box-sizing: border-box;\">Digital Thermometer with Analog Display Measures Air and Water Temperature</li>\r\n	<li style=\"box-sizing: border-box;\">Accurate Digital Compass with Analog Display</li>\r\n	<li style=\"box-sizing: border-box;\">Adjustable Declination Angle for Accuracy</li>\r\n	<li style=\"box-sizing: border-box;\">Water resistant to 330 feet(100 M)</li>\r\n</ul>', '6', 'E1', '5', 1, '0', NULL, NULL, NULL, NULL, '2018-08-10 06:30:27', '2018-08-14 07:03:36'),
(8, 'LM001', 'Lenovo Mobile', '6787', '6500', 'img/product/5b72cbe0291f6-5.jpg', 'img/product/5b72cbe03e443-5.jpg', 'img/product/5b72cbe056390-5.jpg', 'New Prodcut Lakme', '<p>Detail</p>', '2', 'Lenovo Mobile', '1', 1, '1', NULL, NULL, NULL, NULL, '2018-08-14 06:47:32', '2018-08-14 06:47:40'),
(9, '54321', 'abc', '2000', '1500', 'img/product/5b72d37cd421c-octane.jpg', 'img/product/5b72d37ce9899-bolano.jpg', 'img/product/5b72d37d0cd51-bolano.jpg', 'Timex T49859 Intelligent Quartz Compass Tide Temperature Watch For Men', '<p>a</p>\r\n\r\n<p>a</p>\r\n\r\n<p>a</p>\r\n\r\n<p>&nbsp;</p>', '3', 'D1', '4', 4, '1', NULL, NULL, NULL, NULL, '2018-08-14 07:20:01', '2018-08-14 08:36:09'),
(10, '11111', 'Mobile', '50000', '45000', 'img/product/5b72d4983e92d-refelx.jpg', 'img/product/5b72d49852d09-refelx.jpg', 'img/product/5b72d4986b954-refelx.jpg', 'Timex T49859 Intelligent Quartz Compass Tide Temperature Watch For Men', '<p>q</p>\r\n\r\n<p>q</p>\r\n\r\n<p>&nbsp;</p>', '2', 'Lenovo Mobile1', '1', 1, '1', NULL, NULL, NULL, NULL, '2018-08-14 07:24:44', '2018-08-14 07:25:21'),
(11, 'RC1001', 'Rice Cooker', '3000', '2800', 'img/product/5b73fb37540fc-rice-cooker.jpg', 'img/product/5b73fb3823b49-rice-cooker.jpg', 'img/product/5b73fb383b764-rice-cooker.jpg', 'Rice Cooker RC1001', '<ul>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">Brand: Phillips</span></li>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">2.2 L Capacity</span></li>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">Anodized aluminium cooking pan</span></li>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">With scoop holder</span></li>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">Auto-cook</span></li>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">Easy to use</span></li>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">Keep-warm function 4 hours</span></li>\r\n	<li><span style=\"color: rgb(34, 34, 34); font-family: Roboto, sans-serif; font-size: 13.5px;\">Warranty: 1 year</span></li>\r\n</ul>', '8', 'Rice Cooker 01', '2', 3, '1', NULL, NULL, NULL, NULL, '2018-08-15 04:21:48', '2018-08-15 04:21:57'),
(12, 'SAM 2002', 'Laptop', '50000', '45000', 'img/product/5b73fe23a2e79-samsung.jpg', 'img/product/5b73fe23be888-samsung.jpg', 'img/product/5b73fe23d514a-samsung.jpg', 'Samsung i5 5559', '<ul style=\"margin: 0px; padding-right: 0px; padding-left: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 17px;\">\r\n	<li style=\"margin: 0px 0px 0px 17px; padding: 0px; list-style: outside;\">High compatibility</li>\r\n	<li style=\"margin: 0px 0px 0px 17px; padding: 0px; list-style: outside;\">Portable</li>\r\n	<li style=\"margin: 0px 0px 0px 17px; padding: 0px; list-style: outside;\">Platform independent</li>\r\n	<li style=\"margin: 0px 0px 0px 17px; padding: 0px; list-style: outside;\">Low maintenance</li>\r\n	<li style=\"margin: 0px 0px 0px 17px; padding: 0px; list-style: outside;\">Long working life</li>\r\n</ul>', '4', 'S1', '1', 2, '1', NULL, NULL, NULL, NULL, '2018-08-15 04:34:16', '2018-08-15 04:34:23');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `category`, `status`, `rank`, `active`, `caption`, `keywords`, `metaTag`, `metaDescription`, `created_at`, `updated_at`) VALUES
(1, 'Computer', '1', '0', 1, NULL, NULL, NULL, NULL, NULL, '2018-08-10 06:55:19'),
(2, 'Kitchen', '1', NULL, 0, 'kitchen', NULL, NULL, NULL, '2018-08-07 06:04:46', '2018-08-10 06:55:21'),
(3, 'Cosmetic', '1', NULL, 0, 'cosmetic', 'cosmetic', 'cosmetic', NULL, '2018-08-07 06:26:41', '2018-08-10 06:55:22'),
(4, 'Other', '1', NULL, 0, NULL, NULL, NULL, NULL, '2018-08-09 12:52:37', '2018-08-10 06:55:24');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `productType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `productType`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Phones & Tablets', 1, '2018-08-13 05:21:54', '2018-08-13 05:23:29'),
(2, 'Computing', 1, '2018-08-13 05:22:30', '2018-08-13 05:23:31'),
(3, 'Appliances', 1, '2018-08-13 05:23:08', '2018-08-13 05:23:32'),
(4, 'Other', 0, '2018-08-13 05:35:46', '2018-08-13 05:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `company`, `logo`, `address`, `contact`, `email`, `google_map`, `facebook`, `youtube`, `google_plus`, `twitter`, `instagram`, `caption`, `keywords`, `metaTag`, `metaDescription`, `created_at`, `updated_at`) VALUES
(1, 'Kite Nepal Pvt Ltd.', 'img/setting/5b6c869c91d33-flag-3d-round-250.png', 'Suryabinayak, Bhaktapur', '9843398399', 'info@kitenepal.com', NULL, 'https:\\\\www.facebook.com\\kitenepal', 'https://www.youtube.com/channel/UCrQl56ihsXGdX0zwTMkPKXg', 'https:\\\\www.plus.google.com\\kitenepal', 'https:\\\\www.twitter.com\\kitenepal', NULL, 'KN Shop', NULL, NULL, NULL, NULL, '2018-08-13 04:16:20');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagefile` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `imagefile`, `link`, `status`, `active`, `rank`, `caption`, `keywords`, `metaTag`, `metaDescription`, `created_at`, `updated_at`) VALUES
(5, 'img/slider/5b6c9f24c0cad-slider1.jpg', 'slider1.jpg', 1, 1, 1, NULL, NULL, NULL, NULL, '2018-08-09 14:23:04', '2018-08-10 03:35:19'),
(6, 'img/slider/5b6c9fae11a39-slider2.jpg', 'slider2.jpg', 1, 0, 1, NULL, NULL, NULL, NULL, '2018-08-09 14:25:22', '2018-08-10 03:35:27'),
(7, 'img/slider/5b6c9fb5ed9ef-slider3.jpg', 'slider3.jpg', 1, 0, 1, NULL, NULL, NULL, NULL, '2018-08-09 14:25:30', '2018-08-10 03:35:37'),
(9, 'img/slider/5b6c9fdbb0113-slider4.jpg', 'slider4.jpg', 1, 0, 1, NULL, NULL, NULL, NULL, '2018-08-09 14:26:07', '2018-08-10 03:35:41'),
(10, 'img/slider/5b6c9fe3ed7f0-slider5.jpg', 'slider5.jpg', 1, 0, 1, NULL, NULL, NULL, NULL, '2018-08-09 14:26:16', '2018-08-10 03:35:52'),
(12, 'img/slider/5b6d58786ae69-laptops.jpg', 'laptops.jpg', 0, NULL, 1, NULL, NULL, NULL, NULL, '2018-08-10 03:33:48', '2018-08-10 05:49:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Prajwol', 'Prajwolghemosu399@gmail.com', '$2y$10$7OechI8rIGMyBy3W7v9faem4aVWCO7ScfqFz9eKSP1tbqevBS4NS6', 'BE8qWjAQKnoHRFx4qT44Xeo3ypXj7Cj7AYOrdp76lPFORAbTlItaVLYF8x7U', NULL, NULL),
(2, 'p', 'P@gmail.com', '$2y$10$TUKGgFdmUBg5d0mz/OaJYO99ikyB5fMsNYMKFBlDlDuvXTUoNzJmy', NULL, '2018-08-07 05:08:21', '2018-08-07 05:08:21'),
(3, 'Prazol', 'a@gmail.com', '$2y$10$YXi3sQ.6B5Jxc9q4NnZnAeM3SLMqwU07fEZ2IDo23U0hAENuo09He', NULL, '2018-08-07 05:54:22', '2018-08-07 05:54:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_logins`
--
ALTER TABLE `customer_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customer_logins`
--
ALTER TABLE `customer_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
